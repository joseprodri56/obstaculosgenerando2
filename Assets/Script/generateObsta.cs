﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class generateObsta : MonoBehaviour
{
    //Numero de objetos creados
    public static int nObj = 0;
    //Prefab
    public GameObject obstacle1;
    public GameObject obstacle2;
    public GameObject obstacle3;
    public GameObject obstacle4;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("generate", 1f, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void generate()
    {
        if (nObj < 5)
        {
            nObj++;
            //print("nObj: "+nObj);
            int i = UnityEngine.Random.Range(0, 4);
            print("Random: "+i);
            if (i == 0)
            {
                GameObject prefabg = Instantiate(obstacle1);
                transformprefab(obstacle1);
            }
            if(i== 1)
            {
                GameObject prefabg = Instantiate(obstacle2);
                transformprefab(obstacle2);
            }
            if (i == 2)
            {
                GameObject prefabg = Instantiate(obstacle3);
                transformprefab(obstacle3);
            }
            if(i== 3)
            {
                GameObject prefabg = Instantiate(obstacle4);
                transformprefab(obstacle4);
            }
        }
        
    }

    private void transformprefab(GameObject robj) //Random Object
    {
        robj.transform.position = new Vector2(UnityEngine.Random.Range(-7f, 7f), UnityEngine.Random.Range(-4f, 4f));
    }
}
