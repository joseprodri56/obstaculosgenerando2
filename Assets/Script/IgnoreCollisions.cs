﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnoreCollisions : MonoBehaviour
{

    GameObject thisgo;
    GameObject father;
    bool active = false;
    // Start is called before the first frame update
    void Start()
    {
        father = RespawnControl.thisgobj;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if(collision.gameObject.tag == "ObstaclePF" && !active)
        //{
        //    print(false);
        //}
        if (collision.gameObject.tag == "ObstaclePF" && active)
        {
            print(true);    
            RespawnControl.ignore(father, collision.gameObject);
            //print("Entro "+collision.name);
        }
        active = true;
    }
}
